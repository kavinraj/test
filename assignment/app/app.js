// define application
var app = angular.module("crudApp", ["ngRoute"]);
app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : "views/grid.html",
        controller : "userController"
    })
    .when("/list", {
        templateUrl : "views/list.html",
        controller : "userController"
    }) 
    .otherwise({
        redirectTo: '/'
      });
    // $locationProvider.html5Mode(true);
});