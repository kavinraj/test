<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package naked-wp
 */

get_header(); ?>

<section class="row">
  <div class="small-12 columns text-center">
    <div class="leader">

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

      <h1><?php the_title(); ?></h1>
      <p><?php the_content(); ?></p>

	<?php endwhile; else : ?>

	  <p><?php _e( 'Sorry, no results found.', 'naked-wp' ); ?></p>

	<?php endif; ?>

    </div>
  </div>
</section>

<?php get_footer(); ?>
