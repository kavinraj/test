<?php
//clean dashboard
function remove_dashboard_widgets() {

	global $wp_meta_boxes;

	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);

	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);

	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_drafts']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);

	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);

}
add_action('wp_dashboard_setup', 'remove_dashboard_widgets' );
// if (!current_user_can('manage_options')) {
// 	add_action('wp_dashboard_setup', 'remove_dashboard_widgets' );
// }
function enable_more_buttons($buttons) {

$buttons[] = 'fontselect';
$buttons[] = 'fontsizeselect';
$buttons[] = 'styleselect';
$buttons[] = 'backcolor';
$buttons[] = 'newdocument';
$buttons[] = 'cut';
$buttons[] = 'copy';
$buttons[] = 'charmap';
$buttons[] = 'hr';
$buttons[] = 'visualaid';

return $buttons;
}
add_filter("mce_buttons_3", "enable_more_buttons");
add_action( 'wp_dashboard_setup', 'register_my_dashboard_widget' );
function register_my_dashboard_widget() {
wp_add_dashboard_widget(
'my_dashboard_widget',
'Website Guidelines',
'my_dashboard_widget_display'
);

}

add_action( 'admin_menu', 'remove_menus' );
function remove_menus(){
	if ( !current_user_can( 'manage_options' ) ) {
		remove_menu_page( 'tools.php' );
		remove_menu_page( 'options-general.php' );
	}
}
add_action( 'admin_menu', 'remove_themecheck', 999 );
function remove_themecheck() {
	if ( !current_user_can( 'manage_options' ) ) {
		remove_submenu_page( 'themes.php', 'themecheck' );
	}
}
// add_action('wp_dashboard_setup', 'remove_dashboard_widgets' );
// function remove_dashboard_widgets() {
// 	global $wp_meta_boxes;
// 	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
// }
// add_action( 'wp_dashboard_setup', 'register_my_dashboard_widget' );
// function register_my_dashboard_widget() {
// 	wp_add_dashboard_widget(
// 		'my_dashboard_widget',
// 		'My Dashboard Widget',
// 		'my_dashboard_widget_display'
// 	);
//}


function my_dashboard_widget_display() {
	 echo 'Hello, I am nake-wp Bot';
    ?>
    <p>Howdy Mr. <a href="#"> User name [ need to get from wp] </a>, welcome to your website back. Here are some quick guidelines to keep in mind when using the site.</p>


	<h4><strong>Plugins</strong></h4>
    <p>If you'd like to use any plugin please let us know first so we can vet it to make sure the code quality is good.</p>

	<h4><strong>Images</strong></h4>
    <p>Please make sure to always add good title and proper alt text to images. This makes single attachment pages better and does some SEO magic as well.</p>

	<h4><strong>Important Links</strong></h4>
    <ul>
		<li><a href='<?php echo admin_url("post-new.php") ?>'>New Post</a></li>
		<li><a href='<?php echo admin_url("profile.php") ?>'>Your Profile</a></li>
    </ul>


    <?php
}
function my_post_guidelines() {

  $screen = get_current_screen();

  if ( 'post' != $screen->post_type )
    return;

  $args = array(
    'id'      => 'my_website_guide',
    'title'   => 'Content Guidelines',
    'content' => '
    	<h3>Website Content Guidelines</h3>
    	<p>All content on this website must be unique. Formatting must be relegated to level 2 headings and down, level 1 headings should not be used in the post content.</p>
    	<p>
		All images should be inserted with the media editor and should have a title, alt text and a descriptive caption.
    	</p>
    ',
  );

  // Add the help tab.
  $screen->add_help_tab( $args );

}

add_action('admin_head', 'my_post_guidelines');
add_filter( 'plugin_action_links', 'bsb_disable_plugin_actions', 10, 4 );

function bsb_disable_plugin_actions( $actions, $plugin_file, $plugin_data, $context ) {
	$plugins = array( 'advanced-custom-fields-pro/acf.php' );
	if ( array_key_exists( 'deactivate', $actions ) && in_array( $plugin_file, $plugins ))
		unset( $actions['deactivate'] );
	return $actions;
}


function annointed_admin_bar_remove() {
        global $wp_admin_bar;

        /* Remove their stuff */
        $wp_admin_bar->remove_menu('wp-logo');
}
add_action('wp_before_admin_bar_render', 'annointed_admin_bar_remove', 0);
