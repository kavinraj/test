-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 10, 2017 at 09:24 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `assignment`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `employee_id` varchar(25) NOT NULL,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `employee_location` varchar(255) NOT NULL,
  `employee_position` varchar(70) NOT NULL,
  `gender` tinyint(2) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `employee_id`, `first_name`, `last_name`, `employee_location`, `employee_position`, `gender`, `created`, `modified`) VALUES
(1, 'Emp 001', 'Abinaya Sundari', 'Baskaran', 'Theni Main Road, Madurai.', 'Software Engineer', 1, '2017-05-10 20:24:38', '2017-05-10 21:12:56'),
(6, 'Emp 002', 'Poorna Kiruthika', 'Muthu', 'Salem, Coimbatore', 'Tester', 1, '2017-05-10 21:13:58', '2017-05-10 21:13:58'),
(7, 'Emp 003', 'Hari Prasath', 'Murugan', 'Chennai, Renault', 'Test Engineer', 0, '2017-05-10 21:14:47', '2017-05-10 21:14:47'),
(8, 'Emp 004', 'Anitha Krishnan', 'Yokesh', 'Karur, Coimbatore', 'Software Engineer Trainee', 1, '2017-05-10 21:15:39', '2017-05-10 21:15:39'),
(9, 'Emp 004', 'Bala Barath', 'Baskaran', 'Orissa', 'Exceutive Engineer', 0, '2017-05-10 21:16:14', '2017-05-10 21:16:14'),
(10, 'Emp 006', 'Juliet Rani', 'Abraham', 'Coimbatore, Angler', 'Project Leader', 1, '2017-05-10 21:17:19', '2017-05-10 21:17:19'),
(11, 'Emp 009', 'Pavithra', 'Pavithra', 'Pollachi, Coimbatore', 'Software Engineer', 1, '2017-05-10 21:17:53', '2017-05-10 21:17:53'),
(12, 'Emp 010', 'Shahina Begam', 'Abdul', 'Gandhipuram, Coimbatore', 'Software Developer', 1, '2017-05-10 21:18:39', '2017-05-10 21:18:59'),
(13, 'Emp 011', 'Girija Balasubramaniam', 'Murugan', 'Siganallore, Coimbatore', 'Project Leader', 1, '2017-05-10 21:20:10', '2017-05-10 21:20:10'),
(14, 'Emp 013', 'Vanitha Meenu', 'Kuppuswamy', 'Karur, Tiruppur', 'Software Engineer', 1, '2017-05-10 21:21:14', '2017-05-10 21:21:14'),
(15, 'Emp018', 'Lalitha Baskaran', 'Baskaran', 'Arapalayam, Madurai', 'Software Engineer', 1, '2017-05-10 21:22:10', '2017-05-10 21:22:10');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
