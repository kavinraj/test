
app.controller("userController", function($scope,$http,UserService) {
    $scope.users = [];
    $scope.tempUserData = {};
    $scope.currentPage = 0;
    $scope.pageSize = 10;
    // function to get records from the database
    $scope.getRecords = function(){

      //alert(Math.ceil(3 / 10) * 10);
        // $http.get('action.php', {
        //     params:{
        //         'type':'view'
        //     }
        // }).success(function(response){
        //     if(response.status == 'OK'){
        //         $scope.users = response.records;

        //  $scope.perPage(Math.ceil($scope.users.length / 10) * 10);
        //     }
        // });
        UserService.getData().then(function(data) {

    $scope.users = data.data.records;
   
    $scope.perPage(Math.ceil($scope.users.length / 10) * 10);
});

    };
    
    // function to insert or update user data to the database
    $scope.saveUser = function(type){
         var data = $.param({
            'data':$scope.tempUserData,
            'type':type
        });
        
        UserService.saveData(data).then(function(response) { 
var response = response.data;
 if(response){
                if(type == 'edit'){
                    $scope.users[$scope.index].id = $scope.tempUserData.id;
                    $scope.users[$scope.index].employee_id = $scope.tempUserData.employee_id;
                    $scope.users[$scope.index].first_name = $scope.tempUserData.first_name;
                    $scope.users[$scope.index].last_name = $scope.tempUserData.last_name;
                    $scope.users[$scope.index].employee_location = $scope.tempUserData.employee_location;
                    $scope.users[$scope.index].employee_position = $scope.tempUserData.employee_position;
                    $scope.users[$scope.index].gender = $scope.tempUserData.gender;
                    $scope.users[$scope.index].created = $scope.tempUserData.created;
                }else{
                    $scope.users.push({
                        id:response.data.id,
                        employee_id:response.data.employee_id,
                        first_name:response.data.first_name,
                        last_name:response.data.last_name,
                        employee_location:response.data.employee_location,
                        employee_position:response.data.employee_position,
                        gender:response.data.gender,
                        created:response.data.created
                    });
                  
                  $scope.perPage(Math.ceil($scope.users.length / 10) * 10);
       
                }
                $scope.userForm.$setPristine();
                $scope.tempUserData = {};
                $('.formData').slideUp();
                $scope.messageSuccess(response.msg);
            }else{
                $scope.messageError(response.msg);
            }
        });
       
        
        // $http.post("action.php", data, config).success(function(response){
        //     if(response.status == 'OK'){
        //         if(type == 'edit'){
        //             $scope.users[$scope.index].id = $scope.tempUserData.id;
        //             $scope.users[$scope.index].employee_id = $scope.tempUserData.employee_id;
        //             $scope.users[$scope.index].first_name = $scope.tempUserData.first_name;
        //             $scope.users[$scope.index].last_name = $scope.tempUserData.last_name;
        //             $scope.users[$scope.index].employee_location = $scope.tempUserData.employee_location;
        //             $scope.users[$scope.index].employee_position = $scope.tempUserData.employee_position;
        //             $scope.users[$scope.index].gender = $scope.tempUserData.gender;
        //             $scope.users[$scope.index].created = $scope.tempUserData.created;
        //         }else{
        //             $scope.users.push({
        //                 id:response.data.id,
        //                 employee_id:response.data.employee_id,
        //                 first_name:response.data.first_name,
        //                 last_name:response.data.last_name,
        //                 employee_location:response.data.employee_location,
        //                 employee_position:response.data.employee_position,
        //                 gender:response.data.gender,
        //                 created:response.data.created
        //             });
                  
        //           $scope.perPage(Math.ceil($scope.users.length / 10) * 10);
       
        //         }
        //         $scope.userForm.$setPristine();
        //         $scope.tempUserData = {};
        //         $('.formData').slideUp();
        //         $scope.messageSuccess(response.msg);
        //     }else{
        //         $scope.messageError(response.msg);
        //     }
        // });
    };
    $scope.num = [];
    $scope.perPage = function(numb) {
       $scope.num = [];
      for($i=10; $i<=numb; $i=$i+10) {
        $scope.num.push($i);
      }   

    }
    // function to add user data
    $scope.addUser = function(){
        $scope.saveUser('add');
    };

    // function to edit user data
    $scope.editUser = function(user){
        $scope.tempUserData = {
                        id:user.id,
                        employee_id:user.employee_id,
                        first_name:user.first_name,
                        last_name:user.last_name,
                        employee_location:user.employee_location,
                        employee_position:user.employee_position,
                        gender:user.gender,
                        created:user.created

        };
        $scope.index = $scope.users.indexOf(user);
        $('.formData').slideDown();
    };
    
    // function to update user data
    $scope.updateUser = function(){
        $scope.saveUser('edit');
    };
    
    // function to delete user data from the database
    $scope.deleteUser = function(user){
        var conf = confirm('Are you sure to delete the user?');
        if(conf === true){
            var data = $.param({
                'id': user.id,
                'type':'delete'    
            });
           
            UserService.deleteData(data).success(function(response){
                if(response){
                    var index = $scope.users.indexOf(user);
                    $scope.users.splice(index,1);
                    $scope.messageSuccess(response.msg);
                    $scope.perPage(Math.ceil($scope.users.length / 10) * 10);
                }else{
                    $scope.messageError(response.msg);
                }
            });
        }
    };

 $scope.activeMenu = 'card';
    $scope.activeMenuFun = function (menu) {
      
   $scope.activeMenu = menu;
    }
    
    // function to display success message
    $scope.messageSuccess = function(msg){
        $('.alert-success > p').html(msg);
        $('.alert-success').show();
        $('.alert-success').delay(5000).slideUp(function(){
            $('.alert-success > p').html('');
        });
        $('#myModal').modal('hide');
    };
    
    // function to display error message
    $scope.messageError = function(msg){
        $('.alert-danger > p').html(msg);
        $('.alert-danger').show();
        $('.alert-danger').delay(5000).slideUp(function(){
            $('.alert-danger > p').html('');
        });
    };
});
